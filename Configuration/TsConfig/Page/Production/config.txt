##################################
## CONFIGURATION FOR COPY PASTE ##
##################################
TCEMAIN.table.pages {
  disablePrependAtCopy = 1
  disableHideAtCopy = 0
}

TCEMAIN.table.tt_content {
  disablePrependAtCopy = 1
  disableHideAtCopy = 0
}

TCEFORM.tt_content.header_layout.altLabels {
      1 = h1
      2 = h2
      3 = h3
      4 = h4
      5 = h5
}

#########################################
## CONFIGURATION FOR RESPONSIVE IMAGES ##
#########################################
## Default Image cropping ##
TCEFORM.sys_file_reference.crop.config.cropVariants {
    ##
    ##  DO NOT CHANGE
    ##  changed 19.02.2020 v9.5.2 (bh)
    ##  set startRatio (selectedRatio) to NaN
    ##  otherwise svg not working because get cropped and processed to png
    ##
    default {
        title = Crop & Focus Image
        selectedRatio = NaN
        allowedAspectRatios {
            NaN {
                title = free
                value = 0.0
            }
            21:5 {
                title = 21:5
                value = 4.2
            }
            21:9 {
                title = 21:9
                value = 2.3333333
            }
            16:9 {
                title = 16:9
                value = 1.7777777
            }
            4:3 {
                title = 4:3
                value = 1.3333333
            }
            3:2 {
                title = 3:2
                value = 1.5
            }
            1:1 {
                title = 1:1
                value = 1
            }
            2:3 {
                title = 2:3
                value = 0.6666666
            }
            3:4 {
                title = 3:4
                value = 0.75
            }
        }
        cropArea {
            x = 0.0,
            y = 0.0,
            width = 1.0,
            height = 1.0,
        }
        focusArea {
            x = 0.33333333333,
            y = 0.33333333333,
            width = 0.33333333333,
            height = 0.33333333333,
        }
    }
}
## Default Image cropping  - END ##